package scipechatsystem.scipechatserver;


public interface User {
	
	// Returns true if the password is valid
	public boolean login(String password);
	
	// Returns true if the user is currently logged
	// in and has been properly logged off
	public boolean logoff();
	
	// Returns the user's email
	public String getEmail();
	
	// Returns true if the old password is authenticated
	// and the new password is valid
	public boolean changePassword(String oldPassword, 
			String newPassword);
		
}
